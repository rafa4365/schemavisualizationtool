# This is React + Typescript Based Schema Visualization Tool



###  Install Node in your system
1) Dowload and install Node in your system from https://nodejs.org/en/download/

###  Steps to Start Project
1) Go to the project folder.
2) Run command "npm install" to install server dependencies.
3) Go to Client Folder and run the command "npm install" to install client dependencies.
4) Go back to main file and run command "npm run dev" to run both server and client.
5) Run the command "./libcurl_example.exe -u localhost:3000 -d dbname" in terminal where libcurl_example.exe is present to further display the ER Diagrams.

###  Key and there meanings

![Reference](/Client/src/images/keymeanings.PNG)

###  Reference Diagram

![GitHub Logo](/Client/src/images/ERD.jpg)
