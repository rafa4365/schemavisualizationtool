/*
 * libcurl_example.c
 *
 *  Created on: 11.05.2020
 *      Author: sklaebe
 */


#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <curl/curl.h>

char *url = NULL;
char *dbname = NULL;
CURLcode send_curl_request(char *content)
{
	CURL *curl = curl_easy_init();
	CURLcode res;
	if(curl) {
		curl_easy_setopt(curl, CURLOPT_URL, url);
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, content);

		/* if we don't provide POSTFIELDSIZE, libcurl will strlen() by
		   itself */
		curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, (long)strlen(content));

		/* Disable SSL for now for portability to Windows */
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);

		/* Perform the request, res will get the return code */
		res = curl_easy_perform(curl);
		/* Check for errors */
		if(res != CURLE_OK)
		  fprintf(stderr, "curl_easy_perform() failed: %s\n",
				  curl_easy_strerror(res));

		/* always cleanup */
		curl_easy_cleanup(curl);
		return res;
	} else return CURLE_FAILED_INIT;
}

int main(int argc, char *argv[])
{
  CURLcode res;

  int opt;

  if (argc < 5) {
	  fprintf(stderr, "error: missing command line arguments\n");
	  fprintf(stderr, "Usage: %s -d dbname -u url\n", argv[0]);
	  return 1;
  }

  while ((opt = getopt(argc, argv, "u:d:")) != -1) {
      switch (opt) {
      case 'u': url = optarg; break;
      case 'd': dbname = optarg; break;
      default:
          fprintf(stderr, "Usage: %s -d dbname -u url\n", argv[0]);
      }
  }

  if (!url || !dbname) {
	  fprintf(stderr, "URL or dbname not set");
	  return 1;
  }

  printf("Database name is %s\n", dbname);
  printf("URL is %s\n", url);
  res = send_curl_request("key=1&table=region");
  res = send_curl_request("key=2&table=region&column=r_regionkey&type=INTEGER");
  res = send_curl_request("key=2&table=region&column=r_name&type=VARCHAR(100)");
  res = send_curl_request("key=2&table=region&column=r_comment&type=VARCHAR(152)");
  res = send_curl_request("key=3&table=region&column1=r_regionkey");

  res = send_curl_request("key=1&table=nation");
  res = send_curl_request("key=2&table=nation&column=n_nationkey&type=INTEGER");
  res = send_curl_request("key=2&table=nation&column=n_name&type=VARCHAR(100)");
  res = send_curl_request("key=2&table=nation&column=n_regionkey&type=INTEGER");
  res = send_curl_request("key=2&table=nation&column=n_comment&type=VARCHAR(152)");
  res = send_curl_request("key=3&table=nation&column1=n_nationkey");

  res = send_curl_request("key=1&table=supplier");
  res = send_curl_request("key=2&table=supplier&column=s_suppkey&type=INTEGER");
  res = send_curl_request("key=2&table=supplier&column=s_name&type=VARCHAR(25)");
  res = send_curl_request("key=2&table=supplier&column=s_address&type=VARCHAR(40)");
  res = send_curl_request("key=2&table=supplier&column=s_nationkey&type=INTEGER");
  res = send_curl_request("key=2&table=supplier&column=s_phone&type=VARCHAR(15)");
  res = send_curl_request("key=2&table=supplier&column=s_acctbal&type=DECIMAL(12,2)");
  res = send_curl_request("key=2&table=supplier&column=s_comment&type=VARCHAR(100)");
  res = send_curl_request("key=3&table=supplier&column1=s_suppkey");

  res = send_curl_request("key=1&table=customer");
  res = send_curl_request("key=2&table=customer&column=c_custkey&type=INTEGER");
  res = send_curl_request("key=2&table=customer&column=c_name&type=VARCHAR(25)");
  res = send_curl_request("key=2&table=customer&column=c_address&type=VARCHAR(40)");
  res = send_curl_request("key=2&table=customer&column=c_nationkey&type=INTEGER");
  res = send_curl_request("key=2&table=customer&column=c_phone&type=VARCHAR(15)");
  res = send_curl_request("key=2&table=customer&column=c_acctbal&type=DECIMAL(12,2)");
  res = send_curl_request("key=2&table=customer&column=c_mktsegment&type=VARCHAR(10)");
  res = send_curl_request("key=2&table=customer&column=c_comment&type=VARCHAR(100)");
  res = send_curl_request("key=3&table=customer&column1=c_custkey");

  res = send_curl_request("key=1&table=part");
  res = send_curl_request("key=2&table=part&column=p_partkey&type=INTEGER");
  res = send_curl_request("key=2&table=part&column=p_name&type=VARCHAR(55)");
  res = send_curl_request("key=2&table=part&column=p_mfgr&type=VARCHAR(25)");
  res = send_curl_request("key=2&table=part&column=p_brand&type=VARCHAR(10)");
  res = send_curl_request("key=2&table=part&column=p_type&type=VARCHAR(25)");
  res = send_curl_request("key=2&table=part&column=p_size&type=INTEGER");
  res = send_curl_request("key=2&table=part&column=p_container&type=VARCHAR(10)");
  res = send_curl_request("key=2&table=part&column=p_retailprice&type=DECIMAL(12,2)");
  res = send_curl_request("key=2&table=part&column=p_comment&type=VARCHAR(100)");
  res = send_curl_request("key=3&table=part&column1=p_partkey");

  res = send_curl_request("key=1&table=partsupp");
  res = send_curl_request("key=2&table=partsupp&column=ps_partkey&type=INTEGER");
  res = send_curl_request("key=2&table=partsupp&column=ps_suppkey&type=INTEGER");
  res = send_curl_request("key=2&table=partsupp&column=ps_availqty&type=INTEGER");
  res = send_curl_request("key=2&table=partsupp&column=ps_supplycost&type=DECIMAL(12,2)");
  res = send_curl_request("key=2&table=partsupp&column=ps_comment&type=VARCHAR(199)");
  res = send_curl_request("key=3&table=partsupp&column1=ps_partkey&column2=ps_suppkey");

  res = send_curl_request("key=1&table=orders");
  res = send_curl_request("key=2&table=orders&column=o_orderkey&type=INTEGER");
  res = send_curl_request("key=2&table=orders&column=o_custkey&type=INTEGER");
  res = send_curl_request("key=2&table=orders&column=o_orderstatus&type=CHAR(1)");
  res = send_curl_request("key=2&table=orders&column=o_totalprice&type=DECIMAL(12,2)");
  res = send_curl_request("key=2&table=orders&column=o_orderdate&type=ANSIDATE");
  res = send_curl_request("key=2&table=orders&column=o_orderpriority&type=VARCHAR(15)");
  res = send_curl_request("key=2&table=orders&column=o_clerk&type=VARCHAR(15)");
  res = send_curl_request("key=2&table=orders&column=o_shippriority&type=INTEGER");
  res = send_curl_request("key=2&table=orders&column=o_comment&type=VARCHAR(79)");
  res = send_curl_request("key=3&table=orders&column1=o_orderkey");

  res = send_curl_request("key=1&table=lineitem");
  res = send_curl_request("key=2&table=lineitem&column=l_orderkey&type=INTEGER");
  res = send_curl_request("key=2&table=lineitem&column=l_partkey&type=INTEGER");
  res = send_curl_request("key=2&table=lineitem&column=l_suppkey&type=INTEGER");
  res = send_curl_request("key=2&table=lineitem&column=l_linenumber&type=INTEGER");
  res = send_curl_request("key=2&table=lineitem&column=l_quantity&type=DECIMAL(12,2)");
  res = send_curl_request("key=2&table=lineitem&column=l_extendedprice&type=DECIMAL(12,2)");
  res = send_curl_request("key=2&table=lineitem&column=l_discount&type=DECIMAL(12,2)");
  res = send_curl_request("key=2&table=lineitem&column=l_tax&type=DECIMAL(12,2)");
  res = send_curl_request("key=2&table=lineitem&column=l_returnflag&type=CHAR(1)");
  res = send_curl_request("key=2&table=lineitem&column=l_linestatus&type=CHAR(1)");
  res = send_curl_request("key=2&table=lineitem&column=l_shipdate&type=ANSIDATE");
  res = send_curl_request("key=2&table=lineitem&column=l_commitdate&type=ANSIDATE");
  res = send_curl_request("key=2&table=lineitem&column=l_receiptdate&type=ANSIDATE");
  res = send_curl_request("key=2&table=lineitem&column=l_shipinstruct&type=CHAR(25)");
  res = send_curl_request("key=2&table=lineitem&column=l_shipmode&type=CHAR(10)");
  res = send_curl_request("key=2&table=lineitem&column=l_comment&type=VARCHAR(44)");
  res = send_curl_request("key=3&table=lineitem&column1=l_orderkey&column2=l_linenumber");

  res = send_curl_request("key=4&table1=nation&column1_1=n_regionkey&table2=region&column2_1=r_regionkey");
  res = send_curl_request("key=4&table1=supplier&column1_1=s_nationkey&table2=nation&column2_1=n_nationkey");
  res = send_curl_request("key=4&table1=customer&column1_1=c_nationkey&table2=nation&column2_1=n_nationkey");
  res = send_curl_request("key=4&table1=partsupp&column1_1=ps_partkey&table2=part&column2_1=p_partkey");
  res = send_curl_request("key=4&table1=partsupp&column1_1=ps_suppkey&table2=supplier&column2_1=s_suppkey");
  res = send_curl_request("key=4&table1=orders&column1_1=o_custkey&table2=customer&column2_1=c_custkey");
  res = send_curl_request("key=4&table1=lineitem&column1_1=l_orderkey&table2=orders&column2_1=o_orderkey");
  res = send_curl_request("key=4&table1=lineitem&column1_1=l_partkey&column1_2=l_suppkey&table2=partsupp&column2_1=ps_partkey&column2_2=ps_suppkey");
  res = send_curl_request("key=4&table1=lineitem&column1_1=l_suppkey&table2=part&column2_1=p_partkey");
  res = send_curl_request("key=4&table1=lineitem&column1_1=l_suppkey&table2=supplier&column2_1=s_suppkey");

  res = send_curl_request("key=5&table=orders&column=o_orderkey&rate=100");
  res = send_curl_request("key=5&table=orders&column=o_custkey&rate=80");
  res = send_curl_request("key=6&table=lineitem&column=l_shipdate&rate=100");
  res = send_curl_request("key=6&table=lineitem&column=l_receiptdate&rate=53.5");
  (void) res;
  return 0;
}
