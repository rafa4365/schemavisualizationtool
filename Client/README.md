# This is React + Typescript Based Schema Visualization Tool




## How to Start Project

1.  Step 1
    * go to the project folder.
2.  Step 2
    * run node server.js to run the server.
3.  Step 3
    * Go to Client Folder and run the command npm install to download dependencies.
4.  Step 4
    *  run the command "./libcurl_example.exe -u localhost:3000 -d" dbname in terminal where libcurl_example.exe is present to further display the ER Diagrams