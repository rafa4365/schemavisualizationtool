import * as React from "react";
import * as _ from "lodash";
import { DefaultNodeModel } from "@projectstorm/react-diagrams";
import { CanvasWidget } from "@projectstorm/react-canvas-core";
import { DemoCanvasWidget } from "./helpers/DemoCanvasWidget";
import { JSCustomNodeModel } from "../Model/JSCustomNodeModel";
import styled from "@emotion/styled";
import "../App.scss";

class ERDCanvas extends React.Component {
  render() {
    return (
      <div className="erdBody">
        <div className="erdHearder">
          {/* <div className="title">Storm React Diagrams - DnD demo</div> */}
        </div>

        <div className="erdContent">
          <div className="erdLayer">
            <DemoCanvasWidget>
              <CanvasWidget engine={this.props.app.getDiagramEngine()} />
            </DemoCanvasWidget>
          </div>
        </div>
      </div>
    );
  }
}

export default ERDCanvas;
