import * as React from "react";
import { TableItem } from '../Model/TableModel';
import { Application } from '../Model/Application';
import ERDCanvas from './ERDCanvas';
import "../App.scss";
interface IMainCanvasProps {
    TableData: Array<TableItem>;
}

interface IMainCanvasState {
    counter?: number;
}
let application = new Application();
export class MainCanvas extends React.PureComponent<IMainCanvasProps, IMainCanvasState> {

    constructor(pProps: IMainCanvasProps) {
        super(pProps);
        this.count = 1;
        this.isPlaying = false;
        this.forwardButton = this.forwardButton.bind(this);
        this.playButton = this.playButton.bind(this);
        this.pauseButton = this.pauseButton.bind(this);
        this.reverseButton = this.reverseButton.bind(this);
        this.forceUpdateHandler = this.forceUpdateHandler.bind(this);


    }

    forceUpdateHandler() {
        this.forceUpdate();
    };
    private count: number;
    private timerId;
    private isPlaying: boolean;

    render() {

        let results = this.props.TableData;
        application.passData(results, this.count);
        return (
            <div className="erdBody">

                <ERDCanvas app={application} />
                <div className="player text-center">
                    <br></br>

                    <button
                        type="button"
                        id="button_bw"
                        className="btn"
                        onClick={() => this.reverseButton()}
                    >
                        <i className="fa fa-backward"></i>
                    </button>
                    {!this.isPlaying? <button
                        type="button"
                        id="button_play"
                        className="btn"
                        onClick={this.playButton}
                    >
                        <i className="fa fa-play"></i>
                    </button> : <button
                        type="button"
                        id="button_pause"
                        className="btn"
                        onClick={this.pauseButton}
                    >
                        <i className="fa fa-pause"></i>
                    </button>}
                    

                    <button
                        type="button"
                        id="button_fw"
                        className="btn"
                        onClick={this.forwardButton}
                    >
                        <i className="fa fa-forward"></i>
                    </button>
                </div>
            </div>


        )
    }

    private forwardButton(): void {
        
        try {
            // console.log("forwardButton() Clicked", this.count);
            this.isPlaying = false;
            clearTimeout(this.timerId);
            if (this.count > 0 && this.count < this.props.TableData.length) {
                this.count = this.count + 1;
                this.forceUpdateHandler();
                // console.log("Number Update", this.count);
            }
        }
        catch (error) {
            console.error(error);
        }

    }
    public Animation(): void {
        
        this.timerId = setInterval(() => {
            console.log("this is animation button");
            if (this.count<this.props.TableData.length) {
                this.count = this.count + 1;
            console.log("Counter is ", this.count);
            this.forceUpdateHandler();
            
            }
            
          }, 1000);
    
}
    public playButton(): void {
        console.log("playButton() Clicked");
        this.count = this.count;
        this.isPlaying = true;
        this.Animation();
        this.forceUpdateHandler();
    }
    public pauseButton(): void {
        console.log("pauseButton() Clicked");
        this.isPlaying = false;
        clearTimeout(this.timerId);
        
        this.forceUpdateHandler();
    }
    public reverseButton(): void {
        try {
            // console.log("reverseButton() Clicked", this.count);
            clearTimeout(this.timerId);
            this.isPlaying = false;
            if (this.count > 1 && this.count <= this.props.TableData.length) {
                this.count = this.count - 1;
                this.forceUpdateHandler();
                // console.log("Number Update", this.count);
            }
        }
        catch (error) {
            console.error(error);
        }

    }
}

export default MainCanvas
