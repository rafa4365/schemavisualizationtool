import * as React from "react";
import { PortWidget } from "@projectstorm/react-diagrams";
import "../../../src/App.scss";
export class JSCustomNodeWidget extends React.Component {
  handleClick = () => {
    console.log("click");
  };

  render() {
    const { tableName, color, tableDetails } = this.props.node.options;

    var Tables = tableDetails[0].map((items) => {
      return items;
    });
    // console.log("this Node Widget", Tables);
    return (
      <div
        className="custom-node"
        style={{ backgroundColor: color }}
        tabIndex="1"
        onClick={this.handleClick}
      >
        <div className="port-content">
          <h4>{tableName}</h4>
        </div>
        <div className="port-content">
          <div>
            <table>
              <thead>
                {/* <tr style={{ justifyItems: "center" }}>
                  <th>{tableName}</th>
                </tr> */}
                <tr>
                  <th>Key</th>
                  <th>Name</th>
                  <th>Type</th>
                  <th>Sort/Unique</th>
                </tr>
              </thead>
              <tbody>
                {Tables.map((data, i) => {
                  // console.log("Table", data);
                  return (
                    <tr key={i}>
                      <td style={{ paddingRight: "10px" }}>
                        {data.PrimaryKey} {data.ForeignKey[0] ? "FK" : ""}
                      </td>
                      {/* <td style={{ paddingRight: '10px' }}>{data.ForeignKey[0]? 'FK': "" } </td> */}
                      <td style={{ paddingRight: "10px" }}>
                        {data.ColumnName}
                      </td>
                      <td style={{ paddingRight: "30px" }}>
                        {data.ColumnType}
                      </td>
                      <td style={{ paddingLeft: "10px" }}>
                        {data.Sortedness || data.Uniqueness}
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
        <div className="port-container">
          <PortWidget
            engine={this.props.engine}
            port={this.props.node.getPort("in")}
          >
            <div className="circle-port" />
          </PortWidget>
          <PortWidget
            engine={this.props.engine}
            port={this.props.node.getPort("out")}
          >
            <div className="circle-port" />
          </PortWidget>
        </div>
      </div>
    );
  }
}
