import React, { Component } from "react";
import socketIOClient from "socket.io-client";
//import './App.css';
import "./App.scss";
import TableModel from "./Model/TableModel";
import MainCanvas from "./components/MainCanvas";
let model: TableModel = new TableModel();

class App extends Component {
  state = {
    loading: true,
    response: [],
  };
  componentDidMount() {
    // console.log("Component Did Mount Function @From App.tsx");
    this.getApiData();
  }

  async getApiData() {
    const ENDPOINT = "http://localhost:3000";
    const socket = await socketIOClient(ENDPOINT);
    socket.on("posts", async (data) => {
      model.processFile(data);
      // console.log("Get Table Response", model.getTables().length);
      const results = await model.getTables();

      if (model.getTables().length === 8) {
        if (results[7].Table.length > 15) {
          this.setState({ loading: false, response: model.getTables() });
        }
      }
    });
  }

  // private testData: Array<TableItem>;
  render() {
    return (
      <div className="App">
        <div className="entirePage">
          {/* Header */}
          <div className="title-header">
            <br></br>
            <h1>DATA VISUALIZATION</h1>
          </div>

          {this.state.loading ? (
            <div className="canvas">
              <br></br>
              <br></br>
              <h1>Loading API....</h1>
            </div>
          ) : (
            <div>
              <div className="canvas">
                <MainCanvas TableData={this.state.response}></MainCanvas>
              </div>
              <br></br>
            </div>
          )}

          <footer className="bg-light py-5">
            <div className="container">
              <div className="small text-center text-muted">
                Copyright © 2020 - TU Ilmenau
              </div>
            </div>
          </footer>
        </div>
      </div>
    );
  }
}

export default App;
