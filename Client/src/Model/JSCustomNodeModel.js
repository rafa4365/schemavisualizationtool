import { DefaultPortModel, NodeModel } from "@projectstorm/react-diagrams";

export class JSCustomNodeModel extends NodeModel {
  constructor(options = {}) {
    // console.log("I am Sending Msg from JSCustomNodeModel", options);

    super({
      ...options,
      type: "js-custom-node",
    });
    this.color = options.color || "rgb(0,192,255)";
    this.tableName = options.tableName;
    this.tableDetails = options.tableDetails;

    this.addPort(
      new DefaultPortModel({
        in: true,
        name: "in",
      })
    );

    this.addPort(
      new DefaultPortModel({
        in: false,
        name: "out",
      })
    );
  }

}
