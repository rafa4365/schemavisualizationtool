import { uniqueId } from 'lodash';


export type TableItem = {
    UniqueId: number;
    key: number;
    TableName: string;
    Table: Array<TableDetails>;
}

// export type TableData = {
//     DetailKey: number;

//     TableDetails: Array<TableDetails>
// }

export type TableDetails = {
    PrimaryKey?: string;
    ForeignKey: Array<RefTo_RefFrom>;
    // ForeignKey?: Array<string>;
    ColumnName: string;
    ColumnType: string;
    Uniqueness?: number;
    Sortedness?: number;
}
export type RefTo_RefFrom = {
    RefTo: string;
    RefFrom: string;
}

class TableModel {
    constructor() {
        this.mTableItems = new Array();
        this.getTables();
    }

    public processFile(pRawData: string): void {
        this.processAPIData(pRawData);
    }

    public returnLengthofTable() {
        // console.log("tTableIds Length ", this.tTableIds.length);
        // console.log("mTable Length ", this.mTableItems.length);
        console.log("i just put this data in mTableItems", this.mTableItems);

        // return this.tTableIds;

    }

    public getTables(): Array<TableItem> {
        return this.mTableItems;
    }

    /*
     d8888b. d8888b. d888888b db    db  .d8b.  d888888b d88888b
     88  `8D 88  `8D   `88'   88    88 d8' `8b `~~88~~' 88'
     88oodD' 88oobY'    88    Y8    8P 88ooo88    88    88ooooo
     88~~~   88`8b      88    `8b  d8' 88~~~88    88    88~~~~~
     88      88 `88.   .88.    `8bd8'  88   88    88    88.
     88      88   YD Y888888P    YP    YP   YP    YP    Y88888P
     */

    private mTableItems: Array<TableItem>;


    private processAPIData(pRawData: string): void {
        const obj = JSON.parse(JSON.stringify(pRawData));
        // console.log("Raw Complete Object", obj);
        // console.log("Raw Data is passed Column", obj.key);
        // console.log("Raw Data is passed Column", obj.column);
        // console.log("MTable Items", this.mTableItems[0]);
        if (obj.key == 1) {
            var tTableData: TableItem = this.initiateEmptyTable();
            tTableData.UniqueId = uniqueId();
            tTableData.key = obj.key;
            tTableData.TableName = obj.table;
            this.mTableItems.push(tTableData);
        }
        else if (obj.key == 2) {
            if (obj.column !== ' ' && obj.column !== undefined) {
                for (let k = 0; k < this.mTableItems.length; k++) {
                    if (this.mTableItems[k].TableName == obj.table) {
                        let tColumn: TableDetails = {
                            PrimaryKey: " ",
                            ForeignKey: [],
                            ColumnName: obj.column,
                            ColumnType: obj.type
                        }
                        this.mTableItems[k].Table?.push(tColumn)
                    }
                }
            }
        }
        else if (obj.key == 3) {
            // console.log("this also have a PRIMARY key against");
            for (let i = 0; i < this.mTableItems.length; i++) {
                for (let k = 0; k < this.mTableItems[i].Table.length; k++) {
                    if (this.mTableItems[i].TableName == obj.table && this.mTableItems[i].Table[k].ColumnName == obj.column1) {
                        // console.log("Exact Row is ", this.mTableItems[i].Table[k]);
                        this.mTableItems[i].Table[k].PrimaryKey = "PK";
                    }
                }
            }
        }
        else if (obj.key == 4) {
            // console.log("this also have a FOREIGN key against", "Table 1", obj.table1, obj.column1_1, "Table 2", obj.table2, obj.column2_1);
            // console.log("this also have a FOREIGN key against", "Table 1", obj.table1, obj.column1_1, obj.column1_2, "Table 2", obj.table2, obj.column2_1, obj.column2_2);

            for (let i = 0; i < this.mTableItems.length; i++) {
                if (this.mTableItems[i].TableName == obj.table1) {
                    for (let j = 0; j < this.mTableItems[i].Table.length; j++) {
                        if (this.mTableItems[i].Table[j].ColumnName == obj.column1_1) {
                            // console.log("FOUND FIST COLUMN", this.mTableItems[i].Table[j].ForeignKey);
                            var trefToFrom: RefTo_RefFrom = {
                                RefTo: obj.table1,
                                RefFrom: obj.table2
                            }
                            this.mTableItems[i].Table[j].ForeignKey.push(trefToFrom);
                        }
                    }
                }
            }

            // if (obj.column1_2 !== ' ' || obj.column1_2 !== undefined) {
            //     console.log("SECOND");
            //     console.log("this also have a FOREIGN key against", "Table 1", obj.table1, obj.column1_1, obj.column1_2, "Table 2", obj.table2, obj.column2_1, obj.column2_2);
            //     for (let i = 0; i < this.mTableItems.length; i++) {
            //         if (this.mTableItems[i].TableName == obj.table1) {
            //             for (let j = 0; j < this.mTableItems[i].Table.length; j++) {

            //                 if (this.mTableItems[i].Table[j].ColumnName == obj.column2_1) {
            //                     console.log("FOUND SECOND COLUMN", this.mTableItems[i].Table[j].ForeignKey);
            //                     var trefToFrom: RefTo_RefFrom = {
            //                         RefTo: obj.table1,
            //                         RefFrom: "YDFLFKDs"
            //                     }
            //                     this.mTableItems[i].Table[j].ForeignKey.push(trefToFrom);
            //                 }
            //             }
            //         }
            //     }

            // }

        }
        else if (obj.key == 5) {
            for (let i = 0; i < this.mTableItems.length; i++) {
                for (let k = 0; k < this.mTableItems[i].Table.length; k++) {
                    if (this.mTableItems[i].TableName == obj.table && this.mTableItems[i].Table[k].ColumnName == obj.column) {
                        // console.log("Exact Row is ", this.mTableItems[i].Table[k]);
                        this.mTableItems[i].Table[k].Uniqueness = obj.rate;
                    }
                }
            }
        }

        else if (obj.key == 6) {
            for (let i = 0; i < this.mTableItems.length; i++) {
                for (let k = 0; k < this.mTableItems[i].Table.length; k++) {
                    if (this.mTableItems[i].TableName == obj.table && this.mTableItems[i].Table[k].ColumnName == obj.column) {
                        // console.log("Exact Row is ", this.mTableItems[i].Table[k]);
                        this.mTableItems[i].Table[k].Sortedness = obj.rate;
                    }
                }
            }
        }

    }

    private initiateEmptyTable(): TableItem {
        let tTable: TableItem = {
            UniqueId: 0,
            key: 0,
            TableName: " ",
            Table: new Array()
        }
        return tTable;
    }

}
export default TableModel