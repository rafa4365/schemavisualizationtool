import * as SRD from "@projectstorm/react-diagrams";
import { JSCustomNodeFactory } from "../components/CustomNode/JSCustomNodeFactory";
import { JSCustomNodeModel } from "./JSCustomNodeModel";
import TableModel from "./TableModel";
import TableItem from "./TableModel";
import { startCase, camelCase } from "lodash";

export class Application {
  constructor() {
    this.diagramEngine = SRD.default();
    this.diagramEngine
      .getNodeFactories()
      .registerFactory(new JSCustomNodeFactory());
  }

  newModel(modArg, numberOfTable) {
    this.activeModel = new SRD.DiagramModel();
    this.diagramEngine.setModel(this.activeModel);

    const nodes = [];
    const linkNode = [];
    const links = [];
    // for (let i = 0; i < modArg.length; i++) {     First Approach
    for (let i = 0; i < numberOfTable; i++) {
      // console.log("Application Model", modArg[i]);

      // Maping Node Details
      const nodeDetails = {
        id: modArg[i].UniqueId,
        tableName: startCase(camelCase(modArg[i].TableName)),
        color: "rgb(0, 192, 255)",

        tableDetails: [modArg[i].Table],
        position: {
          x: i * 10,
          y: i * 10,
        },
      };
      nodes.push(nodeDetails);
      // Mapping Links
      for (let j = 0; j < modArg[i].Table.length; j++) {
        if (modArg[i].Table[j].ForeignKey.length > 0) {
          for (let k = 0; k < modArg[i].Table[j].ForeignKey.length; k++) {
            linkNode.push(modArg[i].Table[j].ForeignKey[k]);
          }
        }
      }
    }

    for (let l = 0; l < linkNode.length; l++) {
      const linkDetails = {
        id: { l },
        name: { l } + "Link",
        nodeOut: {
          id: "0",
          name: startCase(camelCase(linkNode[l].RefFrom)),
        },
        nodeIn: {
          id: "1",
          name: startCase(camelCase(linkNode[l].RefTo)),
        },
      };
      links.push(linkDetails);
    }

    const linkerTo = modArg[7].Table[2].ForeignKey.map((item) => item.RefTo);
    const linkerFrom = modArg[7].Table[2].ForeignKey.map(
      (item) => item.RefFrom
    );


// Positioning Nodes
    
    const allNodeModel = nodes.map((node, index) => {
      const nodeModel = new JSCustomNodeModel(node);
      if (index >= 0 && index <= 3) {
        nodeModel.setPosition(index * 400, index);
        return nodeModel;
      }
      if (index > 3 && index < 5) {
        nodeModel.setPosition(index, index * 50);
        console.log("Index is 4");
        return nodeModel;
      }
      if (index > 4 && index<6) {
        
        nodeModel.setPosition(index * 77, index * 50);
        console.log("Index is 5");
        return nodeModel;
      }
      if (index >5 && index < 7) {
        console.log("This is index 6");
        nodeModel.setPosition(index * 140, index * 50);
        console.log("Index is 5");
        return nodeModel;
      }
      if (index >6 && index < 8) {
        console.log("This is index 6");
        nodeModel.setPosition(index * 178, index * 50);
        console.log("Index is 5");
        return nodeModel;
      }
      else {
        nodeModel.setPosition(index * 10, index * 10);
        return nodeModel;
      }
      
    });

    // link the ports

    const allLinkModel = links.map((link) => {
      const linkModel = new SRD.DefaultLinkModel();
       allNodeModel.map((nodeModel) => {
       if (link.nodeOut.name === nodeModel.tableName) {
          linkModel.setSourcePort(nodeModel.getPort("out"));
        } else if (link.nodeIn.name === nodeModel.tableName) {
          linkModel.setTargetPort(nodeModel.getPort("in"));
        }

        return null;
      });

      return linkModel;
    });

    this.activeModel.addAll(...allNodeModel, ...allLinkModel);
  }
  // newSerializeModel() {
  //     this.activeModel = new SRD.DiagramModel();
  //     this.activeModel.deserializeModel(parsedData, this.diagramEngine);
  //     this.diagramEngine.setModel(this.activeModel);
  // }

  // getActiveDiagram() {
  //     return this.activeModel;
  // }

  passData(tables, numberOfTable) {
    /**
     * @param TableItems Array of able Data and Number of Tables to Show in Canvas
     */

    this.newModel(tables, numberOfTable);
  }

  getDiagramEngine() {
    return this.diagramEngine;
  }
}
